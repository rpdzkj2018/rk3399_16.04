/*
 * Copyright (c) 2017 Fuzhou Rockchip Electronics Co., Ltd
 *
 * This file is dual-licensed: you can use it either under the terms
 * of the GPL or the X11 license, at your option. Note that this dual
 * licensing only applies to this file, and not this project as a
 * whole.
 *
 *  a) This file is free software; you can redistribute it and/or
 *     modify it under the terms of the GNU General Public License as
 *     published by the Free Software Foundation; either version 2 of the
 *     License, or (at your option) any later version.
 *
 *     This file is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 * Or, alternatively,
 *
 *  b) Permission is hereby granted, free of charge, to any person
 *     obtaining a copy of this software and associated documentation
 *     files (the "Software"), to deal in the Software without
 *     restriction, including without limitation the rights to use,
 *     copy, modify, merge, publish, distribute, sublicense, and/or
 *     sell copies of the Software, and to permit persons to whom the
 *     Software is furnished to do so, subject to the following
 *     conditions:
 *
 *     The above copyright notice and this permission notice shall be
 *     included in all copies or substantial portions of the Software.
 *
 *     THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *     EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 *     OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *     NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 *     HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 *     WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *     FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 *     OTHER DEALINGS IN THE SOFTWARE.
 */

/dts-v1/;

#include "dt-bindings/pwm/pwm.h"
#include "rk3399.dtsi"
#include "rk3399-opp.dtsi"
#include <dt-bindings/display/drm_mipi_dsi.h>
#include <dt-bindings/input/input.h>
#include "rk3399-vop-clk-set.dtsi"
//#include "rp_lcd_hdmi.dtsi"
//#include "rp_lcd_mipi_800_1280.dtsi"
//#include "rp_lcd_mipi_2560_1600.dtsi"
#include "rp_lcd_mipi_720_1280.dtsi"
//#include "rp_lcd_mipi_1920_1200.dtsi"
//#include "rp_lcd_edp_1920_1080.dtsi"

/ {
	compatible = "rockchip,rk3399-mid", "rockchip,rk3399";

	chosen {
		bootargs = "earlycon=uart8250,mmio32,0xff1a0000 swiotlb=1";
	};

	cpuinfo {
		compatible = "rockchip,cpuinfo";
		nvmem-cells = <&efuse_id>;
		nvmem-cell-names = "id";
	};

	ramoops_mem: ramoops_mem {
		reg = <0x0 0x110000 0x0 0xf0000>;
		reg-names = "ramoops_mem";
	};

	ramoops {
		compatible = "ramoops";
		record-size = <0x0 0x20000>;
		console-size = <0x0 0x80000>;
		ftrace-size = <0x0 0x00000>;
		pmsg-size = <0x0 0x50000>;
		memory-region = <&ramoops_mem>;
	};

	clkin_gmac: external-gmac-clock {
		compatible = "fixed-clock";
		clock-frequency = <125000000>;
		clock-output-names = "clkin_gmac";
		#clock-cells = <0>;
	};
	
	fiq_debugger: fiq-debugger {
		compatible = "rockchip,fiq-debugger";
		rockchip,serial-id = <2>;
		rockchip,wake-irq = <0>;
		/* If enable uart uses irq instead of fiq */
		rockchip,irq-mode-enable = <1>;
		/* Only 115200 and 1500000 */
		rockchip,baudrate = <115200>;
		pinctrl-names = "default";
		pinctrl-0 = <&uart2c_xfer>;
		interrupts = <GIC_SPI 150 IRQ_TYPE_LEVEL_HIGH 0>;
	};

	reserved-memory {
		#address-cells = <2>;
		#size-cells = <2>;
		ranges;

		drm_logo: drm-logo@00000000 {
			compatible = "rockchip,drm-logo";
			reg = <0x0 0x0 0x0 0x0>;
		};

		stb_devinfo: stb-devinfo@00000000 {
			compatible = "rockchip,stb-devinfo";
			reg = <0x0 0x0 0x0 0x0>;
		};
	};
	
	led-gpio {
		compatible = "led-led";
		breathe-led = <&gpio4 24 GPIO_ACTIVE_HIGH>;
	};
	
	5v_en {
		compatible = "5v-en-gpio";
		power_en-gpio = <&gpio4 30 GPIO_ACTIVE_HIGH>;
		vddio_en = <&gpio4 22 GPIO_ACTIVE_HIGH>;
		uart5_en-gpio = <&gpio4 29 GPIO_ACTIVE_HIGH>;
		hub_rst = <&gpio1 24 GPIO_ACTIVE_HIGH>;
		fan_open = <&gpio1 18 GPIO_ACTIVE_HIGH>;
		status = "okay";
	};
	
	gpio_ctl{
		compatible = "gpio_ctl";
		gpio_ctl_num1 = <&gpio4 21 GPIO_ACTIVE_LOW>;

	};

	rockchip-key {
		compatible = "rockchip,key";
		status = "okay";

		io-channels = <&saradc 1>;
		vol-up-key {
			linux,code = <114>;
			label = "volume down";
			rockchip,adc_value = <1>;
		};

		vol-down-key {
			linux,code = <115>;
			label = "volume up";
			rockchip,adc_value = <170>;
		};
		power-key {
			gpios = <&gpio0 5 GPIO_ACTIVE_LOW>;
			linux,code = <116>;
			label = "power";
			gpio-key,wakeup;
		};
	};

	rt5651-sound {
		compatible = "simple-audio-card";
		simple-audio-card,format = "i2s";
		simple-audio-card,name = "realtek,rt5651-codec";
		simple-audio-card,mclk-fs = <256>;
		simple-audio-card,widgets =
			"Microphone", "Mic Jack",
			"Headphone", "Headphone Jack";
		simple-audio-card,routing =
			"Mic Jack", "MICBIAS1",
			"IN1P", "Mic Jack",
			"Headphone Jack", "HPOL",
			"Headphone Jack", "HPOR";
		simple-audio-card,cpu {
			sound-dai = <&i2s0>;
		};
		simple-audio-card,codec {
			sound-dai = <&rt5651>;
		};
	};
/*
	rk_headset {
		compatible = "rockchip_headset";
		headset_gpio = <&gpio4 28 GPIO_ACTIVE_HIGH>;
	};
*/
	hdmi_sound: hdmi-sound {
//status = "disabled";
		compatible = "simple-audio-card";
		simple-audio-card,format = "i2s";
		simple-audio-card,mclk-fs = <256>;
		simple-audio-card,name = "rockchip,hdmi";

		simple-audio-card,cpu {
			sound-dai = <&i2s2>;
		};
		simple-audio-card,codec {
			sound-dai = <&dw_hdmi_audio>;
		};
	};

	hdmi_codec: hdmi-codec {
		status = "disabled";
		compatible = "simple-audio-card";
		simple-audio-card,format = "i2s";
		simple-audio-card,mclk-fs = <256>;
		simple-audio-card,name = "HDMI-CODEC";

		simple-audio-card,cpu {
			sound-dai = <&i2s2>;
		};

		simple-audio-card,codec {
			sound-dai = <&hdmi>;
		};
	};

	rga: rga@ff680000 {
		compatible = "rockchip,rga2";
		dev_mode = <1>;
		reg = <0x0 0xff680000 0x0 0x1000>;
		interrupts = <GIC_SPI 55 IRQ_TYPE_LEVEL_HIGH 0>;
		clocks = <&cru ACLK_RGA>, <&cru HCLK_RGA>, <&cru SCLK_RGA_CORE>;
		clock-names = "aclk_rga", "hclk_rga", "clk_rga";
		power-domains = <&power RK3399_PD_RGA>;
		dma-coherent;
		status = "okay";
	};

	spdif-sound {
		compatible = "simple-audio-card";
		//status = "okay";
		status = "disabled";

		simple-audio-card,name = "ROCKCHIP,SPDIF";
		simple-audio-card,cpu {
			sound-dai = <&spdif>;
		};
		simple-audio-card,codec {
			sound-dai = <&spdif_out>;
		};
	};

	spdif_out: spdif-out {
		compatible = "linux,spdif-dit";
		status = "okay";
		#sound-dai-cells = <0>;
	};

	dw_hdmi_audio: dw-hdmi-audio {
		status = "okay";
		compatible = "rockchip,dw-hdmi-audio";
		#sound-dai-cells = <0>;
	};

	sdio_pwrseq: sdio-pwrseq {
		compatible = "mmc-pwrseq-simple";
		clocks = <&rk808 1>;
		clock-names = "ext_clock";
		pinctrl-names = "default";
		pinctrl-0 = <&wifi_enable_h>;

		/*
		 * On the module itself this is one of these (depending
		 * on the actual card populated):
		 * - SDIO_RESET_L_WL_REG_ON
		 * - PDN (power down when low)
		 */
		reset-gpios = <&gpio0 10 GPIO_ACTIVE_LOW>; /* GPIO0_B2 */
	};

	sdmmc_sys: sdmmc-sys {				//SD
		compatible = "regulator-fixed";
		regulator-name = "sdmmc_sys";
		enable-active-high;
		gpio = <&gpio0 1 GPIO_ACTIVE_HIGH>;
		pinctrl-names = "default";
		pinctrl-0 = <&sdmmc_power>;
		regulator-always-on;
	};
	
	vcc3v3_sys: vcc3v3-sys {			//Power_33
		compatible = "regulator-fixed";
		regulator-name = "vcc3v3_sys";
		regulator-always-on;
		regulator-boot-on;
		regulator-min-microvolt = <3300000>;
		regulator-max-microvolt = <3300000>;
	};

	vcc5v0_host: vcc5v0-host-regulator {		//USB
		compatible = "regulator-fixed";
		enable-active-high;
		gpio = <&gpio4 25 GPIO_ACTIVE_HIGH>;
		pinctrl-names = "default";
		pinctrl-0 = <&host_vbus_drv>;
		regulator-name = "vcc5v0_host";
		regulator-always-on;
		regulator-boot-on;
		regulator-min-microvolt = <5000000>;
		regulator-max-microvolt = <5000000>;
	};

	vcc5v0_sys: vcc5v0-sys {
		compatible = "regulator-fixed";
		regulator-name = "vcc5v0_sys";
		regulator-always-on;
		regulator-boot-on;
		regulator-min-microvolt = <5000000>;
		regulator-max-microvolt = <5000000>;
	};
	vcc_phy: vcc-phy-regulator {			//NET
		compatible = "regulator-fixed";
		regulator-name = "vcc_phy";
		regulator-always-on;
		regulator-boot-on;
	};
	
	vdd_log: vdd-log {				//LOGO
		compatible = "pwm-regulator";
		pwms = <&pwm2 0 25000 1>;
		regulator-name = "vdd_log";
		regulator-min-microvolt = <800000>;
		regulator-max-microvolt = <1400000>;
		regulator-always-on;
		regulator-boot-on;

		/* for rockchip boot on */
		rockchip,pwm_id= <2>;
		rockchip,pwm_voltage = <1000000>;
	};

	vccadc_ref: vccadc-ref {			//ADC
		compatible = "regulator-fixed";
		regulator-name = "vcc1v8_sys";
		regulator-always-on;
		regulator-boot-on;
		regulator-min-microvolt = <1800000>;
		regulator-max-microvolt = <1800000>;
	};
	wireless-wlan {
		compatible = "wlan-platdata";
		rockchip,grf = <&grf>;
		wifi_chip_type = "ap6356";
		sdio_vref = <1800>;
		WIFI,host_wake_irq = <&gpio0 3 GPIO_ACTIVE_HIGH>; // GPIO0_a3 
		status = "okay";
	};

	wireless-bluetooth {
		compatible = "bluetooth-platdata";
		clocks = <&rk808 1>;
		clock-names = "ext_clock";
		//wifi-bt-power-toggle;
		uart_rts_gpios = <&gpio2 19 GPIO_ACTIVE_LOW>; // GPIO2_C3 
		pinctrl-names = "default", "rts_gpio";
		pinctrl-0 = <&uart0_rts>;
		pinctrl-1 = <&uart0_gpios>;
		//BT,power_gpio  = <&gpio3 19 GPIO_ACTIVE_HIGH>; // GPIOx_xx 
		BT,reset_gpio    = <&gpio0 9 GPIO_ACTIVE_HIGH>; // GPIO0_B1 
		BT,wake_gpio     = <&gpio2 26 GPIO_ACTIVE_HIGH>; // GPIO2_D2 
		BT,wake_host_irq = <&gpio0 4 GPIO_ACTIVE_HIGH>; // GPIO0_A4 
		status = "okay";
	};
};

&cpu_l0 {
	cpu-supply = <&vdd_cpu_l>;
};

&cpu_l1 {
	cpu-supply = <&vdd_cpu_l>;
};

&cpu_l2 {
	cpu-supply = <&vdd_cpu_l>;
};

&cpu_l3 {
	cpu-supply = <&vdd_cpu_l>;
};

&cpu_b0 {
	cpu-supply = <&vdd_cpu_b>;
};

&cpu_b1 {
	cpu-supply = <&vdd_cpu_b>;
};



&emmc_phy {
	status = "okay";
};

&gmac {
	phy-supply = <&vcc_phy>;
	phy-mode = "rgmii";
	clock_in_out = "input";
	snps,reset-gpio = <&gpio3 15 GPIO_ACTIVE_LOW>;
	snps,reset-active-low;
	snps,reset-delays-us = <0 10000 50000>;
	assigned-clocks = <&cru SCLK_RMII_SRC>;
	assigned-clock-parents = <&clkin_gmac>;
	pinctrl-names = "default";
	pinctrl-0 = <&rgmii_pins>;
	tx_delay = <0x28>;
	rx_delay = <0x11>;
	status = "okay";
};


&gpu {
	status = "okay";
	mali-supply = <&vdd_gpu>;
};

&hdmi {
	#address-cells = <1>;
	#size-cells = <0>;
	#sound-dai-cells = <0>;
	status = "okay";
};

&i2c0 {
	status = "okay";
	i2c-scl-rising-time-ns = <168>;
	i2c-scl-falling-time-ns = <4>;
	clock-frequency = <400000>;

	vdd_cpu_b: syr827@40 {
		compatible = "silergy,syr827";
		status = "okay";
		reg = <0x40>;
		vin-supply = <&vcc5v0_sys>;
		regulator-compatible = "fan53555-reg";
		regulator-name = "vdd_cpu_b";
		regulator-min-microvolt = <712500>;
		regulator-max-microvolt = <1500000>;
		regulator-ramp-delay = <1000>;
		pinctrl-0 = <&vsel1_gpio>;
		fcs,suspend-voltage-selector = <1>;
		regulator-always-on;
		regulator-boot-on;
		regulator-initial-state = <3>;
			regulator-state-mem {
			regulator-off-in-suspend;
		};
	};

	vdd_gpu: syr828@41 {
		compatible = "silergy,syr828";
		status = "okay";
		reg = <0x41>;
		vin-supply = <&vcc5v0_sys>;
		regulator-compatible = "fan53555-reg";
		regulator-name = "vdd_gpu";
		regulator-min-microvolt = <712500>;
		regulator-max-microvolt = <1400000>;	//<1500000>;
		regulator-ramp-delay = <1000>;
		pinctrl-0 = <&vsel2_gpio>;
		fcs,suspend-voltage-selector = <1>;
		regulator-always-on;
		regulator-boot-on;
		regulator-initial-state = <3>;
			regulator-state-mem {
			regulator-off-in-suspend;
		};
	};

	rk808: pmic@1b {
		compatible = "rockchip,rk808";
		reg = <0x1b>;
		interrupt-parent = <&gpio1>;
		interrupts = <21 IRQ_TYPE_LEVEL_LOW>;
		pinctrl-names = "default";
		pinctrl-0 = <&pmic_int_l &pmic_dvs2>;
		rockchip,system-power-controller;
		wakeup-source;
		#clock-cells = <1>;
		clock-output-names = "xin32k", "rk808-clkout2";

		vcc1-supply = <&vcc5v0_sys>;
		vcc2-supply = <&vcc5v0_sys>;
		vcc3-supply = <&vcc5v0_sys>;
		vcc4-supply = <&vcc5v0_sys>;
		vcca-supply = <&vcc5v0_sys>;
		vcc6-supply = <&vcc5v0_sys>;
		vcc7-supply = <&vcc5v0_sys>;
		vcc8-supply = <&vcc3v3_sys>;
		vcc9-supply = <&vcc5v0_sys>;
		vcc10-supply = <&vcc5v0_sys>;
		vcc11-supply = <&vcc5v0_sys>;
		vcc12-supply = <&vcc3v3_sys>;
		vddio-supply = <&vcc1v8_pmu>;

		regulators {
			vdd_center: DCDC_REG1 {
				regulator-always-on;
				regulator-boot-on;
				regulator-min-microvolt = <750000>;
				regulator-max-microvolt = <1350000>;
				regulator-ramp-delay = <6001>;
				regulator-name = "vdd_center";
				regulator-state-mem {
					regulator-off-in-suspend;
				};
			};

			vdd_cpu_l: DCDC_REG2 {
				regulator-always-on;
				regulator-boot-on;
				regulator-min-microvolt = <750000>;
				regulator-max-microvolt = <1350000>;
				regulator-ramp-delay = <6001>;
				regulator-name = "vdd_cpu_l";
				regulator-state-mem {
					regulator-off-in-suspend;
				};
			};

			vcc_ddr: DCDC_REG3 {
				regulator-always-on;
				regulator-boot-on;
				regulator-name = "vcc_ddr";
				regulator-state-mem {
					regulator-on-in-suspend;
				};
			};

			vcc_1v8: DCDC_REG4 {
				regulator-always-on;
				regulator-boot-on;
				regulator-min-microvolt = <1800000>;
				regulator-max-microvolt = <1800000>;
				regulator-name = "vcc_1v8";
				regulator-state-mem {
					regulator-on-in-suspend;
					regulator-suspend-microvolt = <1800000>;
				};
			};

			vcc1v8_dvp: LDO_REG1 {
				regulator-always-on;
				regulator-boot-on;
				regulator-min-microvolt = <1800000>;
				regulator-max-microvolt = <1800000>;
				regulator-name = "vcc1v8_dvp";
				regulator-state-mem {
					regulator-off-in-suspend;
				};
			};

			vcc3v0_tp: LDO_REG2 {
				regulator-always-on;
				regulator-boot-on;
				regulator-min-microvolt = <3000000>;
				regulator-max-microvolt = <3000000>;
				regulator-name = "vcc3v0_tp";
				regulator-state-mem {
					//regulator-off-in-suspend;
					regulator-on-in-suspend;
					regulator-suspend-microvolt = <3000000>;
				};
			};

			vcc1v8_pmu: LDO_REG3 {
				regulator-always-on;
				regulator-boot-on;
				regulator-min-microvolt = <1800000>;
				regulator-max-microvolt = <1800000>;
				regulator-name = "vcc1v8_pmu";
				regulator-state-mem {
					regulator-on-in-suspend;
					regulator-suspend-microvolt = <1800000>;
				};
			};

			vcc_sd: LDO_REG4 {
				regulator-always-on;
				regulator-boot-on;
				regulator-min-microvolt = <1800000>;
				regulator-max-microvolt = <3300000>;
				regulator-name = "vcc_sd";
				regulator-state-mem {
					regulator-on-in-suspend;
					regulator-suspend-microvolt = <3300000>;
				};
			};

			vcca3v0_codec: LDO_REG5 {
				regulator-always-on;
				regulator-boot-on;
				regulator-min-microvolt = <3000000>;
				regulator-max-microvolt = <3000000>;
				regulator-name = "vcca3v0_codec";
				regulator-state-mem {
					regulator-off-in-suspend;
				};
			};

			vcc_1v5: LDO_REG6 {
				regulator-always-on;
				regulator-boot-on;
				regulator-min-microvolt = <1500000>;
				regulator-max-microvolt = <1500000>;
				regulator-name = "vcc_1v5";
				regulator-state-mem {
					regulator-on-in-suspend;
					regulator-suspend-microvolt = <1500000>;
				};
			};

			vcca1v8_codec: LDO_REG7 {
				regulator-always-on;
				regulator-boot-on;
				regulator-min-microvolt = <1800000>;
				regulator-max-microvolt = <1800000>;
				regulator-name = "vcca1v8_codec";
				regulator-state-mem {
					regulator-off-in-suspend;
				};
			};

			vcc_3v0: LDO_REG8 {
				regulator-always-on;
				regulator-boot-on;
				regulator-min-microvolt = <3000000>;
				regulator-max-microvolt = <3000000>;
				regulator-name = "vcc_3v0";
				regulator-state-mem {
					regulator-on-in-suspend;
					regulator-suspend-microvolt = <3000000>;
				};
			};

			vcc3v3_s3: SWITCH_REG1 {
				regulator-always-on;
				regulator-boot-on;
				regulator-name = "vcc3v3_s3";
				regulator-state-mem {
					regulator-off-in-suspend;
				};
			};

			vcc3v3_s0: SWITCH_REG2 {
				regulator-always-on;
				regulator-boot-on;
				regulator-name = "vcc3v3_s0";
				regulator-state-mem {
					regulator-off-in-suspend;
				};
			};
		};
	};
};

&i2c1 {
	status = "okay";
	i2c-scl-rising-time-ns = <300>;
	i2c-scl-falling-time-ns = <15>;

	rt5651: rt5651@1a {
		#sound-dai-cells = <0>;
		compatible = "realtek,rt5651";
		reg = <0x1a>;
		clocks = <&cru SCLK_I2S_8CH_OUT>;
		clock-names = "mclk";
		spk-con-gpio = <&gpio0 11 GPIO_ACTIVE_HIGH>;
	};
};

&i2c4 {
	status = "okay";
	i2c-scl-rising-time-ns = <600>;
	i2c-scl-falling-time-ns = <20>;
/*
	max17047@36 {
		status = "okay";
                compatible = "maxim,max17047";
                reg = <0x36>;
         };
*/
        sensor@1d {
		status = "okay";
                compatible = "gs_mma8452";
		pinctrl-names = "default";
		pinctrl-0 = <&mma8452_irq_gpio>;
                reg = <0x1d>;
                type = <2>;
                irq-gpio = <&gpio1 23 IRQ_TYPE_EDGE_FALLING>;
                irq_enable = <1>;
                poll_delay_ms = <30>;
                layout = <4>;
        };
	fusb0: fusb30x@22 {
		compatible = "fairchild,fusb302";
		reg = <0x22>;
		pinctrl-names = "default";
		pinctrl-0 = <&fusb0_int>;
		int-n-gpios = <&gpio1 2 GPIO_ACTIVE_HIGH>;
		status = "okay";
	};
};

&i2c5 {
	status = "disabled";
	i2c-scl-rising-time-ns = <150>;
	i2c-scl-falling-time-ns = <30>;
	clock-frequency = <400000>;

	
};
&i2s0 {
	status = "okay";
	rockchip,i2s-broken-burst-len;
	rockchip,playback-channels = <8>;
	rockchip,capture-channels = <8>;
	#sound-dai-cells = <0>;
};
&i2s2 {
	#sound-dai-cells = <0>;
	status = "okay";
};

&io_domains {
	status = "okay";

	bt656-supply = <&vcc_1v8>;		/* bt656_gpio2ab_ms */
	audio-supply = <&vcca1v8_codec>;	/* audio_gpio3d4a_ms */
	sdmmc-supply = <&vcc_sd>;		/* sdmmc_gpio4b_ms */
	gpio1830-supply = <&vcc_1v8>;		/* gpio1833_gpio4cd_ms */
};

/*
&pcie_phy {
	status = "okay";
};

&pcie0 {
	ep-gpios = <&gpio4 25 GPIO_ACTIVE_HIGH>;
	num-lanes = <4>;
	pinctrl-names = "default";
	pinctrl-0 = <&pcie_clkreqn_cpm>;
	status = "okay";
};
&pcie0 {
	assigned-clocks = <&cru SCLK_PCIEPHY_REF>;
	assigned-clock-parents = <&cru SCLK_PCIEPHY_REF100M>;
	assigned-clock-rates = <100000000>;
	num-lanes = <4>;
	pinctrl-names = "default";
	status = "okay";
};*/
&pmu_io_domains {
	status = "okay";
	pmu1830-supply = <&vcc_1v8>;
};

&pinctrl {
/*
	pcie {
		pcie_drv: pcie-drv {
			rockchip,pins =
				<1 17 RK_FUNC_GPIO &pcfg_pull_none>;
			};
			pcie_3g_drv: pcie-3g-drv {
			rockchip,pins =
				<0 2 RK_FUNC_GPIO &pcfg_pull_up>;
		};
	};*/
	pmic {
		pmic_int_l: pmic-int-l {
			rockchip,pins =
				<1 21 RK_FUNC_GPIO &pcfg_pull_up>;
		};
		
		pmic_dvs2: pmic-dvs2 {
			rockchip,pins =
				<1 18 RK_FUNC_GPIO &pcfg_pull_down>;
		};
		
		vsel1_gpio: vsel1-gpio {
			rockchip,pins =
				<1 17 RK_FUNC_GPIO &pcfg_pull_down>;
		};

		vsel2_gpio: vsel2-gpio {
			rockchip,pins =
				<1 14 RK_FUNC_GPIO &pcfg_pull_down>;
		};		
	};
	mma8452 {
		mma8452_irq_gpio: mma8452-irq-gpio {
			rockchip,pins = <1 23 RK_FUNC_GPIO &pcfg_pull_up>;
		};
	};
	sdio-pwrseq {
		wifi_enable_h: wifi-enable-h {
			rockchip,pins =
				<0 10 RK_FUNC_GPIO &pcfg_pull_none>;
		};
	};

	wireless-bluetooth {
		uart0_gpios: uart0-gpios {
			rockchip,pins =
				<2 19 RK_FUNC_GPIO &pcfg_pull_none>;
		};
	};

	sdmmmc-sys-power {
		sdmmc_power:sdmmc-power{
			rockchip,pins = <0 1 RK_FUNC_GPIO &pcfg_pull_up>;
		};
	};

	usb2 {
		host_vbus_drv: host-vbus-drv {
			rockchip,pins =
				<4 25 RK_FUNC_GPIO &pcfg_pull_none>;
		};
	};

	fusb30x {
		fusb0_int: fusb0-int {
			rockchip,pins = <1 2 RK_FUNC_GPIO &pcfg_pull_up>;
		};
	};
};

&pwm2 {
	status = "okay";
};
/*
&rkvdec {
	status = "okay";
};
*/
&rockchip_suspend {
	status = "okay";
	rockchip,pwm-regulator-config = <
		(0
		| PWM2_REGULATOR_EN
		)
	>;
	rockchip,power-ctrl =
		<&gpio1 17 GPIO_ACTIVE_LOW>,
		<&gpio1 14 GPIO_ACTIVE_HIGH>;
};

&saradc {
	status = "okay";
	vref-supply = <&vccadc_ref>;
};

&sdhci {
	bus-width = <8>;
	keep-power-in-suspend;
	mmc-hs400-1_8v;
	mmc-hs400-enhanced-strobe;
	non-removable;
	status = "okay";
	supports-emmc;
};
&sdmmc {
	max-frequency = <150000000>;
	supports-sd;
	bus-width = <4>;
	cap-mmc-highspeed;
	cap-sd-highspeed;
	disable-wp;
	num-slots = <1>;
	vqmmc-supply = <&sdmmc_sys>;
	pinctrl-names = "default";
	pinctrl-0 = <&sdmmc_clk &sdmmc_cmd &sdmmc_cd &sdmmc_bus4>;
	status = "okay";
};

&sdio0 {
	max-frequency = <50000000>;
	supports-sdio;
	bus-width = <4>;
	disable-wp;
	cap-sd-highspeed;
	keep-power-in-suspend;
	mmc-pwrseq = <&sdio_pwrseq>;
	non-removable;
	num-slots = <1>;
	pinctrl-names = "default";
	pinctrl-0 = <&sdio0_bus4 &sdio0_cmd &sdio0_clk>;
	sd-uhs-sdr104;
	status = "okay";
};

&spdif {
	status = "okay";
//	pinctrl-0 = <&spdif_bus_1>;
	pinctrl-0 = <&spdif_bus>;	
	i2c-scl-rising-time-ns = <450>;
	i2c-scl-falling-time-ns = <15>;
	#sound-dai-cells = <0>;
};

&tcphy0 {
	extcon = <&fusb0>;
	status = "okay";
};

&tcphy1 {
	status = "okay";
};


//temperature
//ctl open
&threshold {
	temperature = <75000>;
};
//ctl max
&target {
	temperature = <100000>;
};
//ctl soft power off
&soc_crit {
	temperature = <105000>;
};
&tsadc {
	// tshut mode 0:CRU 1:GPIO 
	rockchip,hw-tshut-mode = <1>;
	// tshut polarity 0:LOW 1:HIGH 
	rockchip,hw-tshut-polarity = <1>;
	rockchip,hw-tshut-temp = <110000>;
	status = "okay";
};


&u2phy0 {
	status = "okay";
	extcon = <&fusb0>;

	u2phy0_otg: otg-port {
		status = "okay";
	};

	u2phy0_host: host-port {
		phy-supply = <&vcc5v0_host>;
		status = "okay";
	};
};

&u2phy1 {
	status = "okay";

	u2phy1_otg: otg-port {
		status = "okay";
	};

	u2phy1_host: host-port {
		phy-supply = <&vcc5v0_host>;
		status = "okay";
	};
};

&uart0 {
	pinctrl-names = "default";
	pinctrl-0 = <&uart0_xfer &uart0_cts>;
	status = "okay";
};

&uart2 {
	status = "okay";
};
&uart4 {
	status = "okay";
};
&usbdrd3_0 {
	status = "okay";
	extcon = <&fusb0>;
};

&usbdrd3_1 {
	status = "okay";
};

&usbdrd_dwc3_0 {
	dr_mode = "otg";
	status = "okay";
};

&usbdrd_dwc3_1 {
	status = "okay";
	dr_mode = "host";
};
&usb_host0_ehci {
	status = "okay";
};

&usb_host0_ohci {
	status = "okay";
};

&usb_host1_ehci {
	status = "okay";
};

&usb_host1_ohci {
	status = "okay";
};

/*
&pvtm {
	status = "okay";
};

&pmu_pvtm {
	status = "okay";
};
*/
&vpu {
	status = "okay";
};

&isp0 {
	status = "okay";
};

&isp0_mmu {
	status = "okay";
};

&isp1 {
	status = "okay";
};

&isp1_mmu {
	status = "okay";
};
